![Como Jugar](img/titulo.png)

## Presentado por
![Como Jugar](img/integrantes.png)

## Enunciado

El juego del triqui (entrada de Wikipedia) es bien conocido por la mayoría de las personas. Además, existen algoritmos bien conocidos para jugar automáticamente este juego

El proyecto de este semestre será el desarrollo de un jugador automático (algoritmo) de 3D-tic-tac-toe, una versión 3D del triqui. Esta versión se juega en un tablero tridimensional cuadrado de tamaño n, donde n puede ser 3, 4, 5 o 6. La idea del juego es la misma: lograr hacer una línea de n símbolos asociados al jugador; donde las líneas pueden ser horizontales, verticales, de profundidad y diagonales.

## Interfaz (Primera Entrega)
Crear una interfaz que permita a dos humanos jugar 3D-tic-tac-toe. Se recomienda que esta interfaz sea lo más sencilla, desde el punto de vista técnico, posible. Puede inspirarse en la interfaz propuesta para el juego de la consola Atari 2600.

## Jugador (Segunda Entrega)
Escribir e implementar un algoritmo que juege automáticamente a 3D-tic-tac-toe que pueda remplazar a un humano en la interfaz desarrollada en el punto anterior.

## Estructura de Archivos

1. img -> Contiene las imagenes en formato .png utilizadas para desarrollar la interfaz
2. iu -> Contiene las pantallas en formato .ui utilizadas para desarrollar la interfaz
3. backend.py -> Contine el código utilizado para implementar el algoritmo
4. frontend.py -> Contine el código utilizado para implementar la interfaz
5. propuestaInterfaz.tex -> Contine el documento en .tex de la primera entrega
6. entregaFinal.tex -> Contine el documento en .tex de la segunda entrega
7. Proyecto__Propuesta_de_interfaz___3D_Tic_Tac_Toe.pdf -> Contine el documento en .pdf de la primera entrega
8. Proyecto__Entrega_final___3D_Tic_Tac_Toe.pdf -> Contine el documento en .pdf de la segunda entrega

## Como jugar
![Como Jugar](img/comoJugar.png)

