## =========================================================================
## PROYECTO TIC TAC TOE - backend
## @author 
## Juan Sebastián Barreto Jimenez (juan_barreto@javeriana.edu.co)
## Janet Chen He (j-chen@javeriana.edu.co)
## =========================================================================

## -------------------------------------------------------------------------
import numpy as np
## -------------------------------------------------------------------------

'''
createArray: Creation of the arrays to represent the board
@inputs: n, board size, same value for all dimensions.
@outputs: array, board with -1 in all positions.
'''
def createArray(n):
    array = np.empty((n, n, n), int)
    for k in range(n):
        for i in range(n):
            for j in range(n):
                array[k][i][j] = -1
    return array


'''
case_1: Check case one
@inputs:
        array, to represent the board.
        n, the size of the board.
        pos_i, position in i to evaluate.
        pos_k, position in k to evaluate.
        player, the player identifier.
@outputs: 
        count, points.
        k, None if the cell is not -1, and the position k if the cell is -1.
        i, None if the cell is not -1, and the position i if the cell is -1.
        j, None if the cell is not -1, and the position j if the cell is -1.
'''
def case_1(array,n,pos_i,pos_k,player):
    count=0
    k = i = j = None
    for m in range(n):
        if array[pos_k][pos_i][m] == player:
            count += 1
        if array[pos_k][pos_i][m] == -1:
            k = pos_k
            i = pos_i 
            j = m
    if count == n-1:
        return count, k, i, j
    return count, None,None,None

'''
case_2: Check case two
@inputs:
        array, to represent the board.
        n, the size of the board.
        pos_j, position in j to evaluate.
        pos_k, position in k to evaluate.
        player, the player identifier.
@outputs: 
        count, points.
        k, None if the cell is not -1, and the position k if the cell is -1.
        i, None if the cell is not -1, and the position i if the cell is -1.
        j, None if the cell is not -1, and the position j if the cell is -1.
'''
def case_2(array,n,pos_j,pos_k,player):
    count=0
    k = i = j = None
    for m in range(n):
        if array[pos_k][m][pos_j] == player:
            count += 1
        if array[pos_k][m][pos_j] == -1:
            k = pos_k
            i = m
            j = pos_j
    if count == n-1:
        return count, k, i, j
    return count, None,None,None

'''
case_3: Check case three
@inputs:
        array, to represent the board.
        n, the size of the board.
        pos_k, position in k to evaluate.
        player, the player identifier.
@outputs: 
        count, points.
        k, None if the cell is not -1, and the position k if the cell is -1.
        i, None if the cell is not -1, and the position i if the cell is -1.
        j, None if the cell is not -1, and the position j if the cell is -1.
'''
def case_3(array,n,pos_k,player):
    count=0
    k = i = j = None
    for m in range(n):
        if array[pos_k][m][m] == player:
            count += 1
        if array[pos_k][m][m] == -1:
            k = pos_k
            i = m 
            j = m
    if count == n-1:
       return count, k, i, j
    return count, None,None,None

'''
case_4: Check case four
@inputs:
        array, to represent the board.
        n, the size of the board.
        pos_k, position in k to evaluate.
        player, the player identifier.
@outputs: 
        count, points.
        k, None if the cell is not -1, and the position k if the cell is -1.
        i, None if the cell is not -1, and the position i if the cell is -1.
        j, None if the cell is not -1, and the position j if the cell is -1.
'''
def case_4(array,n,pos_k,player):
    count=0
    aux = n
    k = i = j = None
    for m in range(n):
        aux -= 1
        if array[pos_k][m][aux] == player:
            count += 1
        if array[pos_k][m][aux] == -1:
            k = pos_k
            i = m 
            j = aux
    if count == n-1:
        return count, k, i, j
    return count, None,None,None

'''
case_5: Check case five
@inputs:
        array, to represent the board.
        n, the size of the board.
        pos_i, position in i to evaluate.
        pos_j, position in j to evaluate.
        player, the player identifier.
@outputs: 
        count, points.
        k, None if the cell is not -1, and the position k if the cell is -1.
        i, None if the cell is not -1, and the position i if the cell is -1.
        j, None if the cell is not -1, and the position j if the cell is -1.
'''
def case_5(array,n,pos_i,pos_j,player):
    count=0
    k = i = j = None
    for m in range(n):
        if array[m][pos_i][pos_j] == player:
            count += 1
        if array[m][pos_i][pos_j] == -1:
            k = m
            i = pos_i 
            j = pos_j
    if count == n-1:
        return count, k, i, j
    return count, None,None,None

'''
case_6: Check case six
@inputs:
        array, to represent the board.
        n, the size of the board.
        player, the player identifier.
@outputs: 
        count, points.
        k, None if the cell is not -1, and the position k if the cell is -1.
        i, None if the cell is not -1, and the position i if the cell is -1.
        j, None if the cell is not -1, and the position j if the cell is -1.
'''
def case_6(array,n,player):
    count=0
    k = i = j = None
    for m in range(n):
        if array[m][m][m] == player:
            count += 1
        if array[m][m][m] == -1:
            k = m
            i = m
            j = m
    if count == n-1:
        return count, k, i, j
    return count, None,None,None

'''
case_7: Check case seven
@inputs:
        array, to represent the board.
        n, the size of the board.
        player, the player identifier.
@outputs: 
        count, points.
        k, None if the cell is not -1, and the position k if the cell is -1.
        i, None if the cell is not -1, and the position i if the cell is -1.
        j, None if the cell is not -1, and the position j if the cell is -1.
'''
def case_7(array,n,player):
    count=0
    aux = n
    k = i = j = None
    for m in range(n):
        aux -= 1
        if array[m][m][aux] == player:
            count += 1
        if array[m][m][aux] == -1:
            k = m
            i = m
            j = aux
    if count == n-1:
        return count, k, i, j
    return count, None,None,None

'''
case_8: Check case eight
@inputs:
        array, to represent the board.
        n, the size of the board.
        pos_i, position in i to evaluate.
        player, the player identifier.
@outputs: 
        count, points.
        k, None if the cell is not -1, and the position k if the cell is -1.
        i, None if the cell is not -1, and the position i if the cell is -1.
        j, None if the cell is not -1, and the position j if the cell is -1.
'''
def case_8(array,n,pos_i,player):
    count =0
    k = i = j = None
    for m in range(n):
        if array[m][pos_i][m] == player:
            count += 1
        if array[m][pos_i][m] == -1:
            k = m
            i = pos_i 
            j = m
    if count == n-1:
        return count , k, i, j
    return count , None,None,None

'''
case_9: Check case nine
@inputs:
        array, to represent the board.
        n, the size of the board.
        pos_j, position in j to evaluate.
        player, the player identifier.
@outputs: 
        count, points.
        k, None if the cell is not -1, and the position k if the cell is -1.
        i, None if the cell is not -1, and the position i if the cell is -1.
        j, None if the cell is not -1, and the position j if the cell is -1.
'''
def case_9(array,n,pos_j,player):
    count =0
    k = i = j = None
    for m in range(n):
        if array[m][m][pos_j] == player:
            count += 1
        if array[m][m][pos_j] == -1:
            k = m
            i = m
            j = pos_j
    if count  == n-1:
        return count , k, i, j
    return count , None,None,None

'''
decreasing: Evaluate the decreasing cases
@inputs:
        array, to represent the board.
        n, the size of the board.
        point, to represent the points of the players.
        player, the player identifier.
        pos_i, position in i to evaluate.
        pos_j, position in j to evaluate.
        pos_k, position in k to evaluate.
@outputs: 
        aux_k, None if the cell is not -1, and the position k if the cell is -1.
        aux_i, None if the cell is not -1, and the position i if the cell is -1.
        aux_j, None if the cell is not -1, and the position j if the cell is -1.
'''
def decreasing(array,n,point,player,pos_i,pos_j,pos_k):
    aux_k = aux_i = aux_j = None
    point[0][player],k,i,j = case_1(array,n,pos_i,pos_k,player)
    if point[0][player] == n-1:
        aux_k = k
        aux_i = i
        aux_j = j
    point[1][player],k,i,j = case_2(array,n,pos_j,pos_k,player)
    if point[1][player] == n-1:
        aux_k = k
        aux_i = i
        aux_j = j
    point[2][player],k,i,j = case_3(array,n,pos_k,player)
    if point[2][player] == n-1:
        aux_k = k
        aux_i = i
        aux_j = j
    point[4][player],k,i,j = case_5(array,n,pos_i,pos_j,player)
    if point[4][player] == n-1:
        aux_k = k
        aux_i = i
        aux_j = j
    point[5][player],k,i,j = case_6(array,n,player)
    if point[5][player] == n-1:
        aux_k = k
        aux_i = i
        aux_j = j
    point[7][player],k,i,j = case_8(array,n,pos_i,player)
    if point[7][player] == n-1:
        aux_k = k
        aux_i = i
        aux_j = j
    point[8][player],k,i,j = case_9(array,n,pos_j,player)
    if point[8][player] == n-1:
        aux_k = k
        aux_i = i
        aux_j = j
    return aux_k,aux_i,aux_j

'''
increasing: Evaluate the increasing cases
@inputs:
        array, to represent the board.
        n, the size of the board.
        point, to represent the points of the players.
        player, the player identifier.
        pos_i, position in i to evaluate.
        pos_j, position in j to evaluate.
        pos_k, position in k to evaluate.
@outputs: 
        aux_k, None if the cell is not -1, and the position k if the cell is -1.
        aux_i, None if the cell is not -1, and the position i if the cell is -1.
        aux_j, None if the cell is not -1, and the position j if the cell is -1.
'''
def increasing(array,n,point,player,pos_i,pos_j,pos_k):
    aux_k = aux_i = aux_j = None
    point[0][player],k,i,j = case_1(array,n,pos_i,pos_k,player)
    if point[0][player] == n-1:
        aux_k = k
        aux_i = i
        aux_j = j
    point[1][player],k,i,j = case_2(array,n,pos_j,pos_k,player)
    if point[1][player] == n-1:
        aux_k = k
        aux_i = i
        aux_j = j
    point[3][player],k,i,j = case_4(array,n,pos_k,player)
    if point[3][player] == n-1:
        aux_k = k
        aux_i = i
        aux_j = j
    point[4][player],k,i,j = case_5(array,n,pos_i,pos_j,player)
    if point[4][player] == n-1:
        aux_k = k
        aux_i = i
        aux_j = j
    point[6][player],k,i,j = case_7(array,n,player)
    if point[6][player] == n-1:
        aux_k = k
        aux_i = i
        aux_j = j
    point[7][player],k,i,j = case_8(array,n,pos_i,player)
    if point[7][player] == n-1:
        aux_k = k
        aux_i = i
        aux_j = j
    point[8][player],k,i,j = case_9(array,n,pos_j,player)
    if point[8][player] == n-1:
        aux_k = k
        aux_i = i
        aux_j = j
    return aux_k,aux_i,aux_j

'''
border: Evaluate the border cases
@inputs:
        array, to represent the board.
        n, the size of the board.
        point, to represent the points of the players.
        player, the player identifier.
        pos_i, position in i to evaluate.
        pos_j, position in j to evaluate.
        pos_k, position in k to evaluate.
@outputs: 
        aux_k, None if the cell is not -1, and the position k if the cell is -1.
        aux_i, None if the cell is not -1, and the position i if the cell is -1.
        aux_j, None if the cell is not -1, and the position j if the cell is -1.
'''
def border(array,n,point,player,pos_i,pos_j,pos_k):
    aux_k = aux_i = aux_j = None
    point[0][player],k,i,j = case_1(array,n,pos_i,pos_k,player)
    if point[0][player] == n-1:
        aux_k = k
        aux_i = i
        aux_j = j
    point[1][player],k,i,j = case_2(array,n,pos_j,pos_k,player)
    if point[1][player] == n-1:
        aux_k = k
        aux_i = i
        aux_j = j
    point[4][player],k,i,j = case_5(array,n,pos_i,pos_j,player)
    if point[4][player] == n-1:
        aux_k = k
        aux_i = i
        aux_j = j
    point[7][player],k,i,j = case_8(array,n,pos_i,player)
    if point[7][player] == n-1:
        aux_k = k
        aux_i = i
        aux_j = j
    point[8][player],k,i,j = case_9(array,n,pos_j,player)
    if point[8][player] == n-1:
        aux_k = k
        aux_i = i
        aux_j = j
    return aux_k,aux_i,aux_j

'''
winState: Evaluate if a player won
@inputs:
        point, to represent the points of the players.
        n, the size of the board.
@outputs: 
        win, value of if a player won.
'''
def winState(point,n):
    win = False
    for i in range(9):
        for j in range(2):
            if (point[i][j] == n):
                win = True
    return win

'''
TicTacToe: Game
@inputs:
        array, to represent the board.
        pos_i, position in i to evaluate.
        pos_j, position in j to evaluate.
        pos_k, position in k to evaluate.
        n, the size of the board.
        player, the player identifier.
        point, to represent the points of the players.
@outputs: 
        array, to represent the board.
        win, value of if a player won.
        point, to represent the points of the players.
        aux_k, None if the cell is not -1, and the position k if the cell is -1.
        aux_i, None if the cell is not -1, and the position i if the cell is -1.
        aux_j, None if the cell is not -1, and the position j if the cell is -1.
'''
def TicTacToe(array,pos_i,pos_j,pos_k,n,player,point):
    array[pos_k][pos_i][pos_j] = player
    aux_k=aux_i=aux_j=None
    if pos_i == pos_j:
        k,i,j = decreasing(array,n,point,player,pos_i,pos_j,pos_k)
        if k != None:
            aux_k = k
            aux_i = i
            aux_j = j
    if pos_i + pos_j == n - 1:
        k,i,j = increasing(array,n,point,player,pos_i,pos_j,pos_k)
        if k != None:
            aux_k = k
            aux_i = i
            aux_j = j
    if pos_i != pos_j and pos_i + pos_j != n - 1:
        k,i,j = border(array,n,point,player,pos_i,pos_j,pos_k)
        if k != None:
            aux_k = k
            aux_i = i
            aux_j = j
    win = winState(point,n)
    return array, win, point, aux_k,aux_i,aux_j 
    
