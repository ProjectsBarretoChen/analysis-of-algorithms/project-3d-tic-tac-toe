## =========================================================================
## PROYECTO TIC TAC TOE - backend
## @author 
## Juan Sebastián Barreto Jimenez (juan_barreto@javeriana.edu.co)
## Janet Chen He (j-chen@javeriana.edu.co)
## =========================================================================

## -------------------------------------------------------------------------
import sys
from PyQt5 import uic
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
import numpy as np
import random
import backend
## -------------------------------------------------------------------------

class MainWindow(QMainWindow):
    def __init__(self,player,n,array,win,point,k,i,j,modeGame):
        super().__init__()
        self.player = player
        self.n = n
        self.array = array
        self.win = win
        self.point = point
        self.k = k
        self.i = i
        self.j = j
        self.modeGame = modeGame
        self.mode()
    
    def mode(self):
        self.player = 1
        self.n = 0
        self.array = []
        self.win = False
        self.point = np.zeros((9,2), int)
        self.move(650,40)
        uic.loadUi("./ui/mode.ui", self)
        self.btnJugador.clicked.connect(self.board)
        self.btnJugadores.clicked.connect(self.board)
        self.btnJugar.clicked.connect(self.howPlay)
    
    def howPlay(self):
        uic.loadUi("./ui/howPlay.ui", self)
        self.volver.clicked.connect(self.mode)

    def board(self):
        btnGame = self.sender()
        name = btnGame.objectName()
        if name == f"btnJugador":
            self.modeGame = 1
        else:
            self.modeGame = 2
        uic.loadUi("./ui/board.ui", self)
        self.b3.setIcon(QIcon(QPixmap("./img/Op_3x3.png")))
        self.b3.setIconSize(QSize(140, 140))
        self.b3.clicked.connect(self.game)

        self.b4.setIcon(QIcon(QPixmap("./img/Op_4x4.png")))
        self.b4.setIconSize(QSize(140, 140))
        self.b4.clicked.connect(self.game)

        self.b5.setIcon(QIcon(QPixmap("./img/Op_5x5.png")))
        self.b5.setIconSize(QSize(140, 140))
        self.b5.clicked.connect(self.game)

        self.b6.setIcon(QIcon(QPixmap("./img/Op_6x6.png")))
        self.b6.setIconSize(QSize(140, 140))
        self.b6.clicked.connect(self.game)

        self.volver.clicked.connect(self.mode)


    def buttonPosition(self, k, i, j):
        button_name = f"b{k}{i}{j}"
        return self.findChild(QPushButton, button_name)
    
    def randomPos(self):
        k = i = j = 0
        while (self.array[k][i][j] != -1):
            k = random.randint(0, self.n - 1)
            i = random.randint(0, self.n - 1)
            j = random.randint(0, self.n - 1)
        return k, i, j

    def click(self):
        btn = self.sender()
        name = btn.objectName()
        pos_k = int(name[1])
        pos_i = int(name[2])
        pos_j = int(name[3])
        if (self.array[pos_k][pos_i][pos_j] == -1 and self.win == False):
            if self.modeGame == 1:
                if self.player == 1:
                    btn.setIcon(QIcon(QPixmap("./img/X.png")))
                    self.array, self.win, self.point, pos_k, pos_i, pos_j = backend.TicTacToe(self.array,pos_i,pos_j,pos_k,self.n,self.player,self.point)
                    self.player = 0
                    if self.player == 0 and self.win == False:
                        if(self.k != None):
                            if(self.array[self.k][self.i][self.j] == -1):
                                pos_k = self.k
                                pos_i = self.i
                                pos_j = self.j
                        if(pos_k == None):
                            pos_k, pos_i, pos_j = self.randomPos()
                        btn = self.buttonPosition(pos_k, pos_i, pos_j)
                        btn.setIcon(QIcon(QPixmap("./img/O.png")))
                        self.array, self.win, self.point , self.k, self.i, self.j= backend.TicTacToe(self.array,pos_i,pos_j,pos_k,self.n,self.player,self.point)
                        self.player = 1
            if self.modeGame == 2:
                if self.player == 1:
                    btn.setIcon(QIcon(QPixmap("./img/X.png")))
                    self.array, self.win, self.point, pos_k, pos_i, pos_j= backend.TicTacToe(self.array,pos_i,pos_j,pos_k,self.n,self.player,self.point)
                    self.player = 0
                else:
                    btn.setIcon(QIcon(QPixmap("./img/O.png")))
                    self.array, self.win, self.point, pos_k, pos_i, pos_j = backend.TicTacToe(self.array,pos_i,pos_j,pos_k,self.n,self.player,self.point)
                    self.player = 1
        if self.win == True:
            if self.player == 0:
                self.label.setText('Jugador 1')
            else:
                self.label.setText('Jugador 2')

    def buttons(self):
        for k in range(self.n):
            for i in range(self.n):
                for j in range(self.n):
                    button_name = f"b{k}{i}{j}"
                    button = getattr(self, button_name)
                    button.clicked.connect(self.click)

    def exit(self):
        self.close()       

    def game(self):
        btnGame = self.sender()
        name = btnGame.objectName()
        self.n = int(name[1])
        self.array = backend.createArray(self.n)
        self.move(400,40)
        if self.n == 3:
            uic.loadUi("./ui/Op3.ui", self)  
        elif self.n == 4:
            uic.loadUi("./ui/Op4.ui", self)
        elif self.n == 5:
            uic.loadUi("./ui/Op5.ui", self)
        elif self.n == 6:
            uic.loadUi("./ui/Op6.ui", self)
        self.buttons()
        self.salir.clicked.connect(self.exit)
        self.newGame.clicked.connect(self.mode)
     
if __name__ == "__main__":
    main = QApplication(sys.argv)
    array = []
    point = np.zeros((9,2), int)
    window = MainWindow(1,0,array,False,point,0,0,0,0)
    window.show()
    sys.exit(main.exec())